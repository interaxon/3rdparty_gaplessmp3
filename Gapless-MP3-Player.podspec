Pod::Spec.new do |spec|
  spec.name         = 'Gapless-MP3-Player'
  spec.license      = { :type => 'MIT', :text => <<-LICENSE
                          Copyright 2012
                          Permission is granted to...
                          LICENSE
                      }
  spec.platform     = :ios
  spec.ios.deployment_target = "6.0"
  spec.version      = '1.0.0'
  spec.compiler_flags = '-stdlib=libc++'
  spec.homepage     = 'git@bitbucket.org:interaxon/3rdparty_gaplessmp3.git'
  spec.authors      = { 'Dan Reynolds' => 'http://www.musicianeer.com' }
  spec.summary      = 'Library to playback encoded MP audio files with gapless playback support.'
  spec.source       = { :git => 'git@bitbucket.org:interaxon/3rdparty_gaplessmp3.git', :tag => 'v1.0.0' }
  spec.source_files = 'Gapless-MP3-Player-ARC/Gapless-MP3-Player-ARC/AudioPlayer/*.{h,mm,m}'
  spec.framework    = 'OpenAL', 'AudioToolbox'
end
